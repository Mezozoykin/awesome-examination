package awecome.examination.widget;

import android.support.annotation.NonNull;

/**
 * Created by Sasha on 28.01.2018.
 */

public class CatPoint implements Comparable<CatPoint> {
    private float x;
    private float y;
    private float distance;
    private float percent;
    private CatPlace catPlace;

    public CatPoint(float x, float y, CatPlace catPlace) {
        this.x = x;
        this.y = y;
        this.catPlace = catPlace;
    }

    public CatPoint() {
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public CatPlace getCatPlace() {
        return catPlace;
    }

    public void setCatPlace(CatPlace catPlace) {
        this.catPlace = catPlace;
    }

    public CatPoint calcDistance(float toX, float toY){
        float dx = x - toX;
        float dy = y - toY;
        distance = (float) Math.sqrt( (dx * dx) + (dy * dy) );
        return this;
    }

    public CatPoint calcPercent(float distance){
        percent = this.distance/ distance;
        return this;
    }

    @Override
    public int compareTo(@NonNull CatPoint o) {
        return Math.round(distance - o.distance);
    }

    public float getDistance() {
        return distance;
    }

    public float getPercent() {
        return percent;
    }
}
