package awecome.examination.widget;

/**
 * Created by Sasha on 28.01.2018.
 */

public enum CatPlace {

    HEAD("Head"),
    NECK("Neck"),
    FRONT_FOOT("Front foot"),
    REAR_FOOT("Rear foot"),
    TAIL("Tail"),
    BACK("Back"),
    STOMACH("Stomach");

    public final String placeName;

    CatPlace(String placeName) {
        this.placeName = placeName;
    }

    public static CatPlace getCatPlace(String name) {
        for (CatPlace item : values())
            if (item.placeName.equalsIgnoreCase(name))
                return item;
        return null;
    }

    }
