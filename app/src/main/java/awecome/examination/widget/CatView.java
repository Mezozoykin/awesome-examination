package awecome.examination.widget;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import awecome.examination.R;
import io.reactivex.Observable;


/**
 * Created by Sasha on 27.01.2018.
 */

public class CatView extends LinearLayout {
    private static final float srcRatio = 878f / 720f, AVAILABLE_PERCENT = 1.5f;
    private float  imageRatio, diffRatio;
    private int realWidth, realHeight, dx, dy;
    private View rootView;
    private View imageView;
    private TextView textView;
    private RelativeLayout rl;
    private List<CatPoint> points = new ArrayList<>(6);
    private List<CatPlace> selectedPlaces = new ArrayList<>(6);
    private PopupMenu popupMenu;
    private MotionEvent lastTouch;
    private View anchor;

    private void initPlaces(){
        if (points.size() == 0) {
            points.add(new CatPoint(realWidth * 0.1493f + dx, realHeight * 0.4013f + dy, CatPlace.HEAD));
            points.add(new CatPoint(realWidth * 0.9118f + dx, realHeight * 0.2066f + dy, CatPlace.TAIL));
            points.add(new CatPoint(realWidth * 0.5479f + dx, realHeight * 0.4242f + dy, CatPlace.BACK));
            points.add(new CatPoint(realWidth * 0.2764f + dx, realHeight * 0.8264f + dy, CatPlace.FRONT_FOOT));
            points.add(new CatPoint(realWidth * 0.8083f + dx, realHeight * 0.8290f + dy, CatPlace.REAR_FOOT));
            points.add(new CatPoint(realWidth * 0.5472f + dx, realHeight * 0.6749f + dy, CatPlace.STOMACH));
        }
    }

    private void init(final Context context) {
        rootView = inflate(context, R.layout.cat_view, this);
        textView = rootView.findViewById(R.id.textView);
        imageView = rootView.findViewById(R.id.imageView);
        rl = rootView.findViewById(R.id.rl);
        anchor = new View(context);
        anchor.setLayoutParams(new LayoutParams(1,1));
        rl.addView(anchor);

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    lastTouch = motionEvent;
                    Observable.fromIterable(points)
                            .map(i->i.calcDistance(motionEvent.getX(), motionEvent.getY()))
                            .sorted()
                            .toList()
                            .subscribe(catPoints -> processDistances(catPoints));
                }

                return true;
            }
        });
    }

    private void processDistances(List<CatPoint> catPoints) {
        final Float minDistance = catPoints.get(0).getDistance();
        Observable.fromIterable(catPoints)
                .map(i->i.calcPercent(minDistance))
                .filter(i->i.getPercent()<AVAILABLE_PERCENT)
                .toList()
                .subscribe(this::processResults);
    }

    private void processResults(List<CatPoint> catPoints) {
        if (catPoints.size() == 1) {
            updateText(catPoints.get(0));
        } else {

            anchor.setX(lastTouch.getX());
            anchor.setY(lastTouch.getY());

            popupMenu = new PopupMenu(this.getContext(), anchor);
            int i=0;
            for(CatPoint catPoint : catPoints) {
                popupMenu.getMenu().add(0, i, i, catPoint.getCatPlace().placeName);
                i++;
            }
            popupMenu.setOnMenuItemClickListener(menuItem -> {
                updateText(catPoints.get(menuItem.getItemId()));
                return false;
            });
            popupMenu.show();
        }
    }

    private void updateText(CatPoint catPoint){
        if (selectedPlaces.contains(catPoint.getCatPlace())) {
            selectedPlaces.remove(catPoint.getCatPlace());
        } else {
            selectedPlaces.add(catPoint.getCatPlace());
        }
        Observable.fromIterable(selectedPlaces)
                .collectInto(new StringBuilder(), (s, placeName) -> {
                    s.append(placeName.placeName);
                    s.append(", ");
                }).subscribe(stringBuilder -> {
                    String res = stringBuilder.toString();
                    textView.setText(res.substring(0, res.length()-2));
                });

    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        initBounds();
    }

    private void initBounds() {
        int imageWidth = imageView.getWidth();
        int imageHeight = imageView.getHeight();
        imageRatio = imageWidth/imageHeight;
        diffRatio =  imageRatio - srcRatio ;
        if (diffRatio > 0) {
            realWidth =  Math.round(imageHeight * srcRatio);
            realHeight = imageHeight;
            dx =  (imageWidth - realWidth) / 2;
        } else {
            realHeight =  Math.round(imageWidth * (1/srcRatio));
            realWidth = imageWidth;
            dy =  (imageHeight - realHeight) / 2;
        }

        initPlaces();

    }



    public CatView(Context context) {
        super(context);
        init(context);
    }


    public CatView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CatView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CatView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
}
