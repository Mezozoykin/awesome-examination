package awecome.examination;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import awecome.examination.ui.FirstTaskFragment;
import awecome.examination.ui.SecondTaskFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState == null) showFirstFragment();
    }

    private void showFirstFragment() {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new FirstTaskFragment())
                .commit();
    }

    public void showSecondFragment() {
        getFragmentManager().beginTransaction()
                .replace(R.id.container, new SecondTaskFragment())
                .addToBackStack(null)
                .commit();
    }
}
